// ----------------------------------------------------------------------------
// File Name     	: branchpredict.cpp
// Created By     	: Gaurav Belwal
// Description   	: This is a C++ simulator for 2 bit Branch Predictor.
// -----------------------------------------------------------------------

#include<iostream.h>
#include <stdio.h>
#include<fstream.h>


#define MAX_LINE_SIZE 64
#define MAX_BRANCH_ADDRESS 512

#define TAKEN 't'
#define NOT_TAKEN 'n'

typedef enum {STATE_NOT_TAKEN_0, STATE_NOT_TAKEN_1, STATE_TAKEN_2, STATE_TAKEN_3} state;

state address_array[MAX_BRANCH_ADDRESS + 1];


ifstream input_file_handle;
ofstream output_file_handle;

int total_predictions;
int correct_predictions;

void predict(unsigned int from_address,
             unsigned int to_address,
             char branch_status)
{
    state CS = address_array[from_address];
    state temp_state = CS;

    switch (CS) {
        case STATE_NOT_TAKEN_0:
								if (branch_status == TAKEN) CS += 1;
								else correct_predictions += 1;
								break;
        case STATE_NOT_TAKEN_1:
								if (branch_status == TAKEN) CS += 1;
								else CS -= 1;correct_predictions += 1;
								break;
        case STATE_TAKEN_2:
								if (branch_status == TAKEN) CS += 1;correct_predictions += 1;
								else CS -= 1;
								break;
        case STATE_TAKEN_3:
								if (branch_status == TAKEN) correct_predictions += 1;
								else CS -= 1;
								break;
        default:
								break;
    }

    address_array[from_address] = CS;

    if (temp_state == STATE_NOT_TAKEN_0 || temp_state == STATE_NOT_TAKEN_1)
        cout<<output_file_handle<<","<<from_address<<","<<branch_status<<","<<to_address<<","<<temp_state<<" :: PREDICT NOT TAKEN\n";
    else
        //fprintf(output_file_handle, "%u %c %u :: PREDICT TAKEN (%d)\n", from_address, branch_status, to_address, temp_state);
		cout<<output_file_handle<<","<<from_address<<","<<branch_status<<","<<to_address<<","<<temp_state<<" :: PREDICT TAKEN\n";
    total_predictions += 1;
}

void branch_predictor_simulator(state start)
{
    char line[MAX_LINE_SIZE];
    int line_number;
    unsigned int from_address;
    unsigned int to_address;
    char branch_status;

    line_number = 1;

    total_predictions = 0;
    correct_predictions = 0;

    if ((input_file_handle.open("inp_500.txt", ios::in)) == NULL)
        return;

    if ((output_file_handle.open("out_500.txt", ios:::app)) == NULL)
        return;

    int i;
    for (i = 0; i < MAX_BRANCH_ADDRESS + 1; ++i)
        address_array[i] = start;

		while(std::getline(input_file_handle, line) !=NULL){
        int r = sscanf(line, "%u %c %u", &from_address, &branch_status, &to_address);
        if (r == EOF || r < 3 || from_address > 512 || to_address > 512 || branch_status != 't' && branch_status != 'n')
			cout<<stderr<<", error: ignoring line "<<line_number<<" due error(s)\n";
        else
            predict(from_address, to_address, branch_status);
        line_number += 1;
    }

    input_file_handle.close();
}

int main()
{
    branch_predictor_simulator(STATE_TAKEN_3);

    if (total_predictions) {
        cout<<"Total branches: \n"<<total_predictions;
        cout<<"Correct predictions: \n"<<correct_predictions;
		cout<<"Average prediction: %.1f%%\n"<<(float) correct_predictions / total_predictions * 100;
    }

    return 0;
}
